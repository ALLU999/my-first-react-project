const onlyAlphabets = (evt, maxlen) => {
    const { value } = evt.target;
    const code = (evt.which) ? evt.which : evt.keyCode;
    const regex = /^[a-z\s.]$/i;
    const isValid = (regex.test(String.fromCharCode(code)))
    if (!isValid) evt.preventDefault(); //restrict invalid codes
    if (maxlen && value.length === maxlen) evt.preventDefault(); //restrict length
};

export default onlyAlphabets;
