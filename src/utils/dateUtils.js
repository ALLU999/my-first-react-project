import { format } from 'date-fns';

const dateUtils = {
    toFullISODate: (isoString) => {
        const dateOptions = {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
        };
        return new Date(isoString).toLocaleDateString('en-IN', dateOptions);
    },

    toShortISODate: (isoString) => {
        const dateOptions = {
            year: 'numeric',
            month: 'short',
            day: 'numeric'
        };
        return new Date(isoString).toLocaleDateString('en-IN', dateOptions);
    },

    getDateString: (date) => {
        if (date instanceof Date) return format(date, 'yyyy-MM-dd').split('T')[0];
        else return date.split('T')[0];
    },

    toShortISOTime: (isoString) => {
        const timeOptions = {
            hour: '2-digit',
            minute: '2-digit'
        };
        return new Date(isoString).toLocaleTimeString('en-IN', timeOptions);
    },

    getDateStringByRangeType: (date, time_range_type) => {
        if (time_range_type === 'day') {
            return dateUtils.toShortISODate(date)
        }
        else if (time_range_type === 'week') {
            const end_date = new Date(date);
            end_date.setDate(end_date.getDate() + 6)
            const today = new Date();
            if (end_date > today) {
                return `${dateUtils.toShortISODate(date)} - ${dateUtils.toShortISODate(today)}`;
            }
            return `${dateUtils.toShortISODate(date)} - ${dateUtils.toShortISODate(end_date)}`;
        }
        else if (time_range_type === 'month') {
            return new Date(date).toLocaleString('default', { month: 'long' });
        }
        else if (time_range_type === 'year') {
            return new Date(date).getFullYear();
        }
        return date
    },

    // getDateStringFromId: (id) => {
    //     const dateOptions = {
    //         year: 'numeric',
    //         month: 'short',
    //         day: 'numeric',
    //         hour: 'numeric',
    //         minute: 'numeric'
    //     };
    //     const dateString = id.replace('_', ' ');
    //     const dt = dateString.split(/-|\s/);
    //     const date = new Date(dt.slice(0, 3).reverse().join('-') + ' ' + dt[3]);
    //     return date.toLocaleDateString('en-IN', dateOptions);
    // }
};

export default dateUtils;
