const checkScrollContainerEnd = (target) => {
    const difference = target.scrollHeight - target.scrollTop;
    return (Math.floor(difference) === target.clientHeight) || (Math.ceil(difference) === target.clientHeight);
};

export default checkScrollContainerEnd;
