const onlyAlphaNumeric = (evt, maxlen) => {
    const { value } = evt.target;
    const code = (evt.which) ? evt.which : evt.keyCode;
    const invalidCodes = !(code > 47 && code < 58) && !(code > 64 && code < 91) && !(code > 96 && code < 123);

    if (invalidCodes) evt.preventDefault(); //restrict invalid codes
    if (maxlen && value.length === maxlen) evt.preventDefault(); //restrict length
};

export default onlyAlphaNumeric;
