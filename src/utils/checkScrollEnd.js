const checkScrollEnd = () => {
    const isBottom = (window.innerHeight + Math.ceil(window.pageYOffset)) >= document.body.scrollHeight;
    return window.pageYOffset !== 0 && isBottom;
};

export default checkScrollEnd;
