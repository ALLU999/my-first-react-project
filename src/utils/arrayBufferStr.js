
const arrayBufferStr = (buf) => {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
}

export default arrayBufferStr;
