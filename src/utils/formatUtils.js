import React from 'react';

const truncateToTwo = (value) => {
    return `${value}`.match(/^-?\d+(?:\.\d{0,2})?/)[0];
};

const truncateAll = (value) => {
    return `${value}`.split('.')[0];
};

const getSelectedItems = (data) => {
    const selected = [];
    for (let key in data) {
        if (data[key]) selected.push(key);
    }
    return selected;
};

const getHighlightedText = (content, query) => {
    //split on higlight term and include term into parts, ignore case
    const parts = content.split(new RegExp(`(${query})`, 'gi'));
    return parts.map((part, index) => (
        <span key={index} className={`${part.toLowerCase() === query.toLowerCase() ? 'match' : ''}`}>
            {part}
        </span>
    ));
};

export default {
    truncateToTwo,
    truncateAll,
    getSelectedItems,
    getHighlightedText
};
