const getError = (err, defaultMsg) => {
    if (err.response && err.response.data && err.response.data.message) {
        return err.response.data.message;
    } else if (err.message) {
        return err.message;
    } else {
        return defaultMsg ? defaultMsg : 'Something went wrong. Try again later';
    }
};

export default getError;
