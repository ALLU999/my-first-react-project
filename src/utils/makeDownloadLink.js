const makeDownloadLink = (filename, csv) => {
    const csvFile = new Blob([csv], { type: 'text/csv' });
    const dateStr = new Date().toISOString().split('T')[0];

    const link = document.createElement('a');
    link.download = `${filename} - ${dateStr}.csv`;
    link.href = URL.createObjectURL(csvFile);
    link.onclick = (evt) => evt.stopPropagation();
    link.click();
    URL.revokeObjectURL(csvFile);
};

export default makeDownloadLink;
