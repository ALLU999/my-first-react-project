const generateLatLng = (lat, lng, radiusInMeters) => {
    const getRandomCoordinates = (radius, uniform) => {
        //generate two random numbers
        let a = Math.random();
        let b = Math.random();
        //flip for more uniformity.
        if (uniform) {
            if (b < a) {
                let c = b;
                b = a;
                a = c;
            }
        }
        return [
            b * radius * Math.cos(2 * Math.PI * a / b),
            b * radius * Math.sin(2 * Math.PI * a / b)
        ];
    };

    const randomCoordinates = getRandomCoordinates(radiusInMeters, true);
    //earths radius in meters via WGS 84 model.
    const earth = 6378137;
    //offsets in meters.
    const northOffset = randomCoordinates[0];
    const eastOffset = randomCoordinates[1];
    //offset coordinates in radians.
    const offsetLatitude = northOffset / earth;
    const offsetLongitude = eastOffset / (earth * Math.cos(Math.PI * (lat / 180)));

    //offset position in decimal degrees.
    return {
        lat: lat + (offsetLatitude * (180 / Math.PI)),
        lng: lng + (offsetLongitude * (180 / Math.PI))
    };
};

export default generateLatLng;
