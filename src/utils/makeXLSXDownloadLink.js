import fileSaver from 'file-saver';

const makeXLSXDownloadLink = (filename, data) => {
    var xlsxFile = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    fileSaver.saveAs(xlsxFile, `${filename}.xlsx`);
}

export default makeXLSXDownloadLink;
