const isSampleAllowed = (email) => {
    const allowed_ids = [
        'allu.s@lynk.co.in'
    ];
    return allowed_ids.includes(email);
};


// eslint-disable-next-line import/no-anonymous-default-export
export default {
    isSampleAllowed,
};
