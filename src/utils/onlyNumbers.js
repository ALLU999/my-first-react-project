const onlyNumbers = (evt, maxlen) => {
    const { value } = evt.target;
    const code = (evt.which) ? evt.which : evt.keyCode;
    const invalidCodes = code > 31 && (code < 48 || code > 57);

    if (invalidCodes) evt.preventDefault(); //restrict invalid codes
    if (maxlen && value.length === maxlen) evt.preventDefault(); //restrict length
};

export default onlyNumbers;
