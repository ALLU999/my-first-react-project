import axios from 'axios';
import { DUMMY_URL } from '../config/urls';

const getDummyData = async (cancelToken) => {
    try {
        console.log(DUMMY_URL);
        // const response = await axios.get(`${DUMMY_URL}/v1/employees`, {
        const response = await axios.get(`https://fortnite-api.theapinetwork.com/upcoming/get`, {
            cancelToken: cancelToken.token
        });
        if (response.status === 200) {
            return response.data.data;
        }
    } catch (err) {
        throw err;
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getDummyData
};
