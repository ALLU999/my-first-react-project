import React, { useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import Dummy from './pages/Dummy/Dummy';

const Routes = () => {

    const history = useHistory();

    useEffect(() => {
        history.push('/dummy');

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Switch>
            <Route to={"/dummy"} component={Dummy} />
        </Switch>);
}

export default Routes;
