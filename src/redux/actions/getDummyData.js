import axios from 'axios';
import {
    DUMMY_DATA_REQUEST,
    DUMMY_DATA_SUCCESS,
    DUMMY_DATA_EMPTY,
    DUMMY_DATA_ERROR
} from './types';
import { dummyAPI } from '../../api';
//import { transformers } from '../../utils';
//import { RECORDS_LIMIT } from '../../config/data';

const getDummyData = (cancelToken) => {
    return async (dispatch) => {
        try {
            dispatch({ type: DUMMY_DATA_REQUEST });
            const data = await dummyAPI.getDummyData(cancelToken);
            console.log(data);
            if (data.length > 0) {
                dispatch({ type: DUMMY_DATA_SUCCESS, payload: { data, page_no: 0, total: data.length } });
            } else {
                dispatch({ type: DUMMY_DATA_EMPTY });
            }
        } catch (err) {
            if (!axios.isCancel(err)) {
                dispatch({ type: DUMMY_DATA_ERROR });
            }
            throw err;
        }
    };
};

export default getDummyData;
