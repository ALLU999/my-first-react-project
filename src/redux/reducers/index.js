
import { combineReducers } from 'redux';
import dummyReducer from './dummy';
import { RESET_APP_STATE } from '../actions/types';

const appReducer = combineReducers({
    dummy: dummyReducer
})

// eslint-disable-next-line import/no-anonymous-default-export
export default (state, action) => {
    if (action.type === RESET_APP_STATE) {
        state = {};
    }
    return appReducer(state, action);
};
