import {
    DUMMY_DATA_REQUEST,
    DUMMY_DATA_SUCCESS,
    DUMMY_DATA_EMPTY,
    DUMMY_DATA_ERROR,
    RESET_DUMMY_DATA
} from '../../actions/types';
//import { RECORDS_LIMIT } from '../../../config/data';

const initialState = {
    data: [],
    total: 0,
    page_no: 0,
    isFetching: false,
    isEmpty: false,
    isErrored: false
};

const tableReducer = (state = initialState, action) => {
    switch (action.type) {
        case DUMMY_DATA_REQUEST:
            return { ...state, isFetching: true };
        case DUMMY_DATA_SUCCESS:
            let data = [];
            const total = action.payload.total;
            const updatedImageIds = action.payload.data.map((dt) => dt.itemId);
            const notUpdatedData = state.data.filter((dt) => !updatedImageIds.includes(dt.itemId));
            data = [...notUpdatedData, ...action.payload.data];
            return { data, total, page_no: action.payload.page_no, isFetching: false, isEmpty: false, isErrored: false };
        case DUMMY_DATA_EMPTY:
            return { ...state, isFetching: false, isEmpty: true, isErrored: false };
        case DUMMY_DATA_ERROR:
            return { ...state, isFetching: false, isErrored: true };
        case RESET_DUMMY_DATA:
            return { ...state, ...initialState };
        default:
            return state;
    }
};

export default tableReducer;
