
import { combineReducers } from 'redux';
import tableReducer from './tableReducer';
import { RESET_DUMMY_DATA } from '../../actions/types';

const appReducer = combineReducers({
    employees: tableReducer
})

// eslint-disable-next-line import/no-anonymous-default-export
export default (state, action) => {
    if (action.type === RESET_DUMMY_DATA) {
        state = {};
    }
    return appReducer(state, action);
};
