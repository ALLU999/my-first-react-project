import React from 'react';
import { useSelector } from 'react-redux';

const Table = () => {

    const { data: employees, isFetching, page_no } = useSelector((state) => state.dummy.employees);

    return (
        <>
            <div className="daas-table-wrap mt-sm image-approval-table">
                <table className="daas-table">
                    <thead>
                        <tr>
                            <th>ITEM ID</th>
                            <th> Name</th>
                            <th> Description</th>
                            <th> Type</th>
                            <th> Rarity</th>
                            <th> Series</th>
                        </tr>
                    </thead>
                    <tbody>
                        {employees.length ? employees.map((row, index) => {
                            return (<tr key={`${row.itemId}`}>
                                <td> {row.itemId} </td>
                                <td> {row.item.name} </td>
                                <td> {row.item.description} </td>
                                <td> {row.item.type} </td>
                                <td> {row.item.rarity} </td>
                                <td> {row.item.series} </td>
                            </tr>)
                        }) : null}
                    </tbody>
                </table>
                {!isFetching && !employees.length && !page_no && (
                    <div className="empty-table-message">No more data to display</div>
                )}

            </div>
        </>
    );
}
export default Table;
