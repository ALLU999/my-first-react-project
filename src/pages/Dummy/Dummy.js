import React, { useCallback, useMemo } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useMount } from 'react-use';
import { useDispatch } from 'react-redux';
import { getDummyData } from '../../redux/actions';

import Table from './Table';

const Dummy = () => {

    const dispatch = useDispatch();
    const cancelToken = useMemo(() => axios.CancelToken.source(), []);

    useMount(() => {
        handleGetDummyData();
    })

    const handleGetDummyData = useCallback(async () => {
        try {
            await dispatch(getDummyData(cancelToken));
        } catch (err) {
            if (!axios.isCancel(err)) {
                toast.error('Something went wrong. Try again later');
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch]);

    return (
        <Table />
    );
}
export default Dummy;
